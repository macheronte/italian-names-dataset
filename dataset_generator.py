# -*- coding: utf-8 -*-

vowels = ['a', 'e', 'i', 'o', 'u', 'à', 'è', 'é', 'ì', 'ò', 'ù']

def calculate_features(name):
    name = name.lower()
    lenght = len(name)
    
    n_vowels = 0
    n_consonants = 0

    end_a = 0
    end_e = 0
    end_i = 0
    end_o = 0
    end_u = 0

    for i in name:
        if(i in vowels):
            n_vowels += 1
        else:
            n_consonants += 1

    last_letter = name[-1].lower()

    if last_letter == 'a' or last_letter == 'à':
        end_a = 1
    elif last_letter == 'e' or last_letter == 'è' or last_letter == 'é':
        end_e = 1
    elif last_letter == 'i' or last_letter == 'ì':
        end_i = 1
    elif last_letter == 'o' or last_letter == 'ò':
        end_o = 1
    elif last_letter == 'u' or last_letter == 'ù':
        end_u = 1

    return lenght, n_vowels, n_consonants, end_a, end_e, end_i, end_o, end_u

def write_features(source, destination, type):
    try:  
        fp = open(source)
        text = fp.read()

        names = text.split('\n')

        for name in names:
            print("Calculating features for " + name)
            lenght, n_vowels, n_consonants, end_a, end_e, end_i, end_o, end_u = calculate_features(name)

            with open(destination, 'a') as the_file:
                the_file.write('"' + name + '",' + str(lenght) + ',' + str(n_vowels) + ',' + str(n_consonants) 
                               + ',' + str(end_a) + ',' + str(end_e) + ',' + str(end_i) + ',' + str(end_o)
                               + ',' + str(end_u) + ',' + str(type) + '\n')

    finally:  
        fp.close()

def generate_dataset():
    print("Generating dataset...")
    with open('dataset_names.txt', 'w') as the_file:
                the_file.write('"name","length","vowels","consonants","end_a","end_e","end_i","end_o","end_u"\n')

    write_features('males.txt','dataset_names.txt',1)
    write_features('females.txt','dataset_names.txt',-1)


generate_dataset()
print("Dataset generated!")
